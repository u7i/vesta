class Downloads {
    static main(args) {
        const targetResources = app.content.resources
        const targetNames = app.content.tasks.map(_ => _.name)

        Downloads.displayResources(targetResources, targetNames, args.highlight)
    }

    static displayResources(resources, targetNames, highlightedID) {
        // Prepare items tree for the injection to DOM
        let resourcesTree = targetNames.map(it => new Object({
            name: it,
            highlight: false,
            items: []
        }))

        resources.forEach(entry => resourcesTree[entry.ownerId].items.push({
            name: entry.name,
            iconUrl: app.resource(entry.icon),
            link: app.resource(entry.link)
        }))

        if (highlightedID != null) {
            const highlighted = resourcesTree[highlightedID]

            for (let i = highlightedID; i >= 1; --i)
                resourcesTree[i] = resourcesTree[i - 1]

            resourcesTree[0] = highlighted
            resourcesTree[0].highlight = true
        }

        // Filter all uncompleted targets
        resourcesTree = resourcesTree.filter(it => it.items.length > 0)

        // Draw the items tree
        const list = page.getElementById("downloads-list")

        resourcesTree.forEach(it => {
            const group = Downloads.resourceGroupDOM(it.name, it.highlight, it.items)
            list.appendChild(group)
        })
    }

    static resourceGroupDOM(name, highlight, resources) {
        // Create the group name header
        const header = document.createElement("h6")
        header.className = "text-center"
        highlight ? header.classList.add("highlighted") : header.classList.add("text-muted")
        header.innerText = name

        // Create items list
        const itemsList = document.createElement("animated-list")
        itemsList.className = "w-100"

        resources.forEach(_ => {
            const resource = {
                icon: _.iconUrl,
                label: _.name,
                action: "Передивитися",
                onAction: () => window.open(_.link, "_blank")
            }

            itemsList.appendItem(resource)
        })

        // Add bottom margins
        const footer = document.createElement("div")
        footer.className = "mb-2"

        const card = document.createElement("li")
        card.className = "list-group-item d-flex flex-column justify-content-between align-items-center"

        card.appendChild(header)
        card.appendChild(itemsList)
        card.appendChild(footer)

        return card
    }
}