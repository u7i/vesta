Module.import("framework/Network.js");

(() => {
    const self = Module.self()

    window.app = {
        content: JSON.parse(Network.sync([self.relative("../resources/content.json")])),

        main: () => {
            // Create the page-map
            const controller = new PagesController()

            controller.setSelector(document.getElementById("selector"))
            controller.setArea(document.getElementById("drawer"))
            controller.setPageMap(document.getElementById("pages"))

            controller.forceSelect("Головна")
        },

        resource: link => {
            try {
                new URL(link)
                return link
            } catch (_) {
                return self.relative("../resources/" + link)
            }
        },
    }
})()