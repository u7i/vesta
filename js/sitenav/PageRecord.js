/** Page properties. */
class PageRecord {
    name    ///< The page name
    entry   ///< The page entry function
    element ///< The page DOM

    constructor(args) {
        this.name = args.name
        this.entry = args.entry
        this.element = args.element
    }
}