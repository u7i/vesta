Module.import("PageMap.js")
Module.import("PageSelector.js")
Module.import("PageArea.js")

class PagesController {
    #lsSelected
    #lsGoto

    #selected

    #selectorElement
    #areaElement
    #pageMapElement

    constructor() {
        this.#lsSelected = e => this.#loadPageRecord(this.#pageMapElement?.findByName(e.detail.name), {})
        this.#lsGoto = e => this.#loadPageRecord(this.#pageMapElement?.findByName(e.detail.name), e.detail.args)

        this.#selected = { record: null, args: {} }

        this.#selectorElement = null
        this.#areaElement = null
        this.#pageMapElement = null
    }

    setSelector(element) {
        // Release the previous selector
        this.#selectorElement?.removeEventListener("selected", this.#lsSelected)
        this.#selectorElement?.select(null)

        // Set up the new element
        this.#selectorElement = element

        this.#selectorElement.clearPages()
        this.#pageMapElement?.forEach(_ => this.#selectorElement.addPage(_.name))

        this.#selectorElement.select(this.#selected?.name)
        this.#selectorElement.addEventListener("selected", this.#lsSelected)
    }

    setArea(element) {
        // Release the previous drawer
        this.#areaElement?.removeEventListener("goto", this.#lsGoto)
        this.#areaElement?.display(null)

        // Set up the page drawer
        this.#areaElement = element

        this.#areaElement.display(this.#selected?.record, this.#selected?.args)
        this.#areaElement.addEventListener("goto", this.#lsGoto)
    }

    setPageMap(element) {
        this.forceSelect(null)

        // Set up the new context
        this.#pageMapElement = element

        this.#selectorElement?.clearPages()
        this.#pageMapElement.forEach(_ => this.#selectorElement?.addPage(_.name))
    }

    forceSelect(name, args) {
        this.#loadPageRecord(this.#pageMapElement?.findByName(name), args)
    }


    #loadPageRecord(record, args) {
        if (record === this.#selected.record)
            return

        // Warn: Order important
        this.#selected = { record: record, args: args }

        //
        this.#selectorElement?.select(record?.name)
        this.#areaElement?.display(record, args)
    }
}