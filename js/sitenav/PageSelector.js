Module.import("../framework/Component.js")
Module.import("../framework/Event.js");

(() => {
    const self = Module.self()

    /**
     * Animated page selector component.
     * Requires only page names.
     */
    Component.define("page-selector", [
        "https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css",
        self.relative("../../css/component/PageSelector.css")
    ], class extends HTMLElement {
        #onselected
        #selected

        #pagesElements
        #cursorElement
        #pagesListElement

        constructor() {
            super()

            this.#onselected = new Event(this, "selected", "onselected")
            this.#selected = null
            this.#pagesElements = {}

            // Make pages list
            this.#pagesListElement = document.createElement("div")
            this.#pagesListElement.className = "d-flex align-items-center"

            // Make the cursor
            this.#cursorElement = document.createElement("div")
            this.#cursorElement.className = "cursor opacity-0"

            // Build the selector UI
            const container = Component.init(this)
            container.className = "gx-5"

            container.appendChild(this.#pagesListElement)
            container.appendChild(this.#cursorElement)
        }

        connectedCallback() {
            this.#assignCursorToPage(this.#selected)
        }

        /** Adds a new page. */
        addPage(name) {
            console.log(`Added page ${name}`)
            console.trace()

            // Add margin to the previous element
            this.#pagesListElement.lastChild?.classList.add("me-2")

            // Create a new label
            const label = document.createElement("h2")
            label.className = "page"
            label.innerText = name

            label.onclick = () => this.select(name)

            this.#pagesListElement.appendChild(label)

            // Make the page
            this.#pagesElements[name] = label
        }

        /** Removes all pages from the selector. */
        clearPages() {
            this.#pagesListElement.childNodes.forEach(_ => this.#pagesListElement.removeChild(_))
            this.#pagesElements = []

            this.select(null)
        }

        /** Selects a page. */
        select(val) {
            // Find the page by the name
            if (!this.#pagesElements[val] && val)
                throw new ReferenceError(`Page does not exist ( ${val} )`)

            // Select the page ( or remove the selection )
            this.#assignCursorToPage(val)
            this.#selected = val

            // Dispatch an event
            this.#onselected.dispatch({name: val})
        }

        /** Gets the currently selected page */
        selected() {
            return this.#selected
        }

        #assignCursorToPage(page) {
            const cursor = this.#cursorElement
            const pageElement = this.#pagesElements[page]

            if (page == null) {
                cursor.classList.add("opacity-0")
                return
            }

            if (cursor.classList.contains("opacity-0")) {
                cursor.classList.remove("opacity-0") // show the cursor

                cursor.classList.remove("cursor-move-animation") // disable move anims
                cursor.classList.add("cursor-fade-in")

                cursor.onanimationend = () => cursor.classList.remove("cursor-fade-in")
            }

            cursor.style.left = this.#elementBoundingRect(pageElement).x + "px"
            cursor.style.width = this.#elementBoundingRect(pageElement).w + "px"

            cursor.classList.add("cursor-move-animation") // force enable anims
        }

        #elementBoundingRect(element) {
            const parent = {
                x: element.parentElement ? element.parentElement.offsetLeft : 0,
                y: element.parentElement ? element.parentElement.offsetTop : 0
            }

            return {
                x: element.offsetLeft - parent.x,
                y: element.offsetTop - parent.y,
                w: element.getBoundingClientRect().width,
                h: element.getBoundingClientRect().height
            }
        }
    })
})()