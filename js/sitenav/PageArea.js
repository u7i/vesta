Module.import("../framework/Component.js")
Module.import("../framework/Event.js");

(() => {
    const self = Module.self()

    Component.define("page-area", [
        self.relative("../../css/component/PageArea.css")
    ], class extends HTMLElement {
        #ongoto
        #container

        constructor() {
            super()

            this.#ongoto = new Event(this, "goto", "ongoto")

            // Set up the UI
            this.#container = Component.init(this)
        }

        display(record, args) {
            this.#container.innerHTML = ''

            if (!record)
                return

            // Make the page UI ( record element left unchanged )
            const styles = this.#duplicateStylesSheets(this.#container)
            const element = record.element.cloneNode(true)

            this.#container.appendChild(styles)
            this.#container.appendChild(element)

            // Make the context
            window.page = this.shadowRoot
            window.page.goto = (name, args) => this.#ongoto.dispatch({name: name, args: args})

            this.#invokePage(record, args)

            // Play an animation
            this.#slideIn(element)
        }

        #duplicateStylesSheets() {
            let text = ""

            for (const sheet of document.styleSheets) {
                for (const rule of sheet.cssRules)
                    text += rule.cssText + '\n'
            }

            // Inline cloned css
            const style = document.createElement("style")
            style.innerHTML = text

            return style
        }

        #invokePage(record, args) {
            const func = this.#resolveFunction(record.entry)
            throw_if(func == null, SyntaxError, `Page entry was not found ( ${record.name} )`)
            throw_if(typeof func !== "function", SyntaxError, `Page entry must be a function ( ${record.name} )`)

            func(args)
        }

        #resolveFunction(name) {
            try {
                return eval(name)
            } catch (_) {
                return null
            }
        }

        #slideIn(element) {
            element.classList.add("slide-in")
            element.onanimationend = () => element.classList.remove("slide-in")
        }
    })
})()