Module.import("../framework/Component.js")
Module.import("../framework/Utils.js")
Module.import("PageRecord.js")

/**
 * Page-map storage.
 * Parses page records from the child-nodes.
 *
 * How To:
 * - Create a new "page-map" tag on your page
 * - Add "page-container" child tags to it.
 * - Each page-container must have { name, entry } attributes set.
 */
Component.define("page-map", [], class extends HTMLElement {
    #pages

    constructor() {
        super()

        this.#pages = {}

        Component.init(this)

        // Look for children addition
        const observer = new MutationObserver(mutations => {
            mutations.forEach(_ => {
                _.addedNodes.forEach(added => {
                    let node = added
                    while (node && node.tagName !== "PAGE-CONTAINER")
                        node = node.parentNode

                    node ? this.#discoverPage(node) : null
                })
            })
        })
        observer.observe(this, { childList: true, subtree: true })
    }

    connectedCallback() {
        // Hide all pages definitions
        this.style.display = "none"
    }

    /** Finds a record by its name. If nothing was found - returns null. */
    findByName(name) {
        for (const key in this.#pages) {
            if (this.#pages[key].name === name)
                return this.#pages[key]
        }

        return null
    }

    /** Iterate over each page record. */
    forEach(callback) {
        for (const key in this.#pages)
            callback(this.#pages[key])
    }


    #discoverPage(element) {
        // Parse the page attributes
        const name = element.getAttribute("name")
        throw_if(!name, SyntaxError, `Page doesn't have a name ( ${this.id} )`)

        const entry = element.getAttribute("entry")
        throw_if(!entry, SyntaxError, `Page doesn't have an entry point ( ${this.id} )`)

        const container = document.createElement("div")
        container.innerHTML = element.innerHTML

        this.#pages[name] = new PageRecord({
            name: name,
            entry: entry,
            element: container
        })
    }
})

Component.define("page-container", [], class extends HTMLElement {
    constructor() { super() }
})