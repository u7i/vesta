class Main {
    static main() {
        const active = app.content.tasks.filter(_ => !_.completed)
        const completed = app.content.tasks.filter(_ => _.completed)

        Main.displayTasks(active, "active-tasks")
        Main.displayTasks(completed, "completed-tasks")
    }

    static displayTasks(tasks, listId) {
        const list = page.getElementById(listId)

        tasks.forEach(_ =>
            list.appendItem({
                icon: app.resource(_.icon),
                label: _.name,
                action: "Результати",
                onAction: () => page.goto("Завантаження", { highlight: _.id })
            })
        )
    }
}