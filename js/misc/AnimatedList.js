Module.import("../framework/Component.js");

(() => {
    const self = Module.self();

    Component.define("animated-list", [
        "https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css",
        self.relative("../../css/component/AnimatedList.css")
    ], class extends HTMLElement {
        #container

        constructor() {
            super()

            this.#container = Component.init(this)
            this.#container.className = "list-group w-100 p-0"
        }

        connectedCallback() {
            this.childNodes
                .forEach(_ => this.#fadeIn(_))
        }

        appendItem(node) {
            // Create an icon
            const icon = document.createElement("img")
            icon.className = "icon me-3"
            icon.src = node.icon

            // Create a label
            const label = document.createElement("span")
            label.className = "gx-5"
            label.innerText = node.label

            // Create the info layout
            const infoLayout = document.createElement("div")
            infoLayout.className = "d-flex align-items-center"

            infoLayout.appendChild(icon)
            infoLayout.appendChild(label)

            // Create the action button
            let actionBtn = document.createElement("button")
            actionBtn.className = "btn btn-outline-secondary"
            actionBtn.innerText = node.action

            actionBtn.onclick = () => node.onAction()

            // Create an item
            const item = document.createElement("li")
            item.className = "list-group-item list-group-item-light d-flex justify-content-between align-items-center"

            item.appendChild(infoLayout)
            item.appendChild(actionBtn)

            // Add item to the DOM
            this.#container.appendChild(item)
            this.#fadeIn(item)
        }


        #fadeIn(element) {
            element.classList.add("fade-in")
            element.onanimationend = () => element.classList.remove("fade-in")
        }
    })
})()