/**
 * Module context.
 * May be acquired through Module.self() method.
 *
 * How To:
 * - Create context using Module.self() immediately after imports
 * - Optionally use the trick with anonymous functions to hide the context variable.
 * - Every time, when you want to access some file by a relative path use context.relative("path")
 */
class ModuleContext {
    #parentDir

    constructor(parentDir) {
        this.#parentDir = parentDir
    }

    /** Makes a path relative to this module. */
    relative(path) {
        return this.#parentDir + path
    }
}

/**
 * Module importer.
 * Doesn't allow multiple imports of the single module file.
 *
 * How To:
 * - Define in top of your module file imports with "Module.import("bla")".
 * - Include your module to HTML with "<script is="js-module" src="Path to a module"></script>".
 */
class Module {
    static #imported = []
    static #currentDir = ""

    /** Sets the currently processed script' url. */
    static setCurrentScript(url) {
        Module.#currentDir = url.match(/(.*)[\/\\]/)[1]||''
        Module.#currentDir += "/"
    }

    /** Import dependency. Imports each unique only once ( like "#pragma once" in cpp ) */
    static import(script) {
        console.info(`Import ${script} ( currentDir = ${this.#currentDir} )`)

        // Get the absolute path to the script
        const realUrl = new URL(script, Module.#currentDir).href

        // Include each file only once
        if (this.#imported.find(_ => _ === realUrl))
            return

        // Do the sync fetch
        const request = new XMLHttpRequest()
        request.open('GET', realUrl, false)
        request.send()

        if (request.response.error) {
            console.error(`Cannot import ${realUrl}`)
            return
        }

        // Register this module
        this.#imported.push(realUrl)

        // Create the contexts switchers
        const current = `Module.setCurrentScript("${Module.#currentDir}")`
        const target = `Module.setCurrentScript("${realUrl}")`

        // Inject an included module ( with the context toggling )
        const container = document.createElement("script")
        container.innerHTML = `${target}\n`
        container.innerHTML += request.responseText
        container.innerHTML += '\n'
        container.innerHTML += `${current}\n`

        document.head.appendChild(container)
    }

    /** Gets the current module context. MUST only be called immediately after the imports. */
    static self() {
        return new ModuleContext(this.#currentDir)
    }
}

customElements.define("js-module", class extends HTMLScriptElement {
    #wasInvoked

    constructor() {
        super()
        this.#wasInvoked = false
    }

    connectedCallback() {
        const origin = this.#determineOrigin()
        this.#switchContextOnce(origin)
    }

    set innerHTML(val) {
        if (this.#inlined()) {
            const origin = this.#determineOrigin()
            this.#switchContextOnce(origin)
        }

        super.innerHTML = val
    }

    set origin(val) {
        this.setAttribute("origin", val)
    }

    get origin() {
        const relative = this.getAttribute("origin")
        return new URL(relative, document.baseURI).href
    }


    #determineOrigin() {
        // Respects priority set by the "script" tag implementation
        if (this.src) return this.src
        if (this.origin) return this.origin

        return document.location.pathname
    }

    #inlined() {
        return !this.hasAttribute("src")
    }

    #switchContextOnce(origin) {
        if (this.#wasInvoked)
            return

        Module.setCurrentScript(origin)
        this.#wasInvoked = true
    }
}, { extends: "script" })