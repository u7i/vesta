/**
 * Event dispatcher.
 * Works with element fields/attributes and event listeners.
 *
 * How To:
 * - Create new event field
 * - Call dispatch(body) method to invoke an event.
 */
class Event {
    #owner
    #type
    #observed

    constructor(owner, type, observedName) {
        this.#owner = owner
        this.#type = type
        this.#observed = observedName
    }

    /** Dispatch an event with a payload ( detail ). */
    dispatch(detail = null) {
        const prop = this.#owner[this.#observed]
        const attr = this.#owner.getAttribute(this.#observed)

        const callback = prop ? prop : new Function(attr)

        detail ? callback(detail) : callback()
        this.#owner.dispatchEvent(new CustomEvent(this.#type, { "detail": detail }))
    }
}