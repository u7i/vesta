/**
 * Downloads files from the network.
 * Caches every downloaded file, so methods without "d" prefix MUST NOT be used to download any dynamic resources.
 * Do not download huge files with caching because of RAM cache.
 */
class Network {
    static #cache = []

    /** Downloads files in the main thread ( cache is used ). */
    static sync(urls, placeholder = "") {
        return urls.map(url => {
            if (this.#cache[url])
                return this.#cache[url]

            // Actually download the file
            const content = this.dsync([url], null)
            if (!content)
                return placeholder

            this.#cache[url] = content
            return content
        })
    }

    /** Downloads files in the coroutine ( cache is used ). */
    static async(urls, placeholder = "") {
        return urls.map(url => {
            if (this.#cache[url])
                return this.#cache[url]

            // Actually download the file
            this.dasync([url], null)
                .then(content => {
                    if (!content)
                        return placeholder

                    this.#cache[url] = content
                    return content
                })
        })
    }

    /** Downloads files in the main thread ( without using the cache ). */
    static dsync(urls, placeholder = "") {
        return urls.map(url => {
            const {error, content} = this.#fetchSync(url)
            return error ? placeholder : content
        })
    }

    /** Downloads files in the coroutine ( without using the cache ). */
    static dasync(urls, placeholder = "") {
        const files = urls.map(url => {
            Network.#fetchAsync(url)
                .then(_ => _.error ? placeholder : _.content)
        })

        return Promise.all(files)
    }


    static #fetchSync(url) {
        // Download the file content
        const request = new XMLHttpRequest()
        request.open('GET', url, false)

        request.send()

        if (request.response.error)
            return { error: true, content: null }
        else
            return { error: false, content: request.responseText }
    }

    static #fetchAsync(url) {
        return fetch(url)
            .then(_ => _.text())
            .then(_ => { return { error: false, content: _ } })
            .catch(_ => { return { error: true, content: null } })
    }
}