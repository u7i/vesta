Module.import("Network.js")

/**
 * Component is an abstraction over HTML custom elements.
 * Allows to easily inject styles into the shadowDOM.
 *
 * How To:
 * - Define the component in raw js script ( ES modules won't work )
 * - Component class must be derived from HTMLElement
 * - In constructor component MUST invoke init method to initialize the shadowDOM and inject all styles into it.
 * - Component can construct DOM in the "container" returned from the init method, but must not access "container" parent.
 */
class Component {
    static #registry = []

    /** Defines a new component class. Downloads all the component resources synchronously, so may take a long time. */
    static define(name, css, cls) {
        customElements.define(name, cls)

        const tagName = name.toUpperCase()

        Component.#registry[tagName] = {}
        Component.#registry[tagName].styles = Network.sync(css)
    }

    /** Creates the component shadowDOM. Returns component root element ( container ). */
    static init(component) {
        const record = Component.#registry[component.tagName]
        if (!record)
            throw new TypeError("Component wasn't defined")

        // Inject stylesheets
        const styleElements = record.styles.map(_ => {
            const style = document.createElement("style")
            style.innerHTML = _

            return style
        })

        // Make the container
        const container = document.createElement("div")

        // Make the shadow DOM
        component.attachShadow({ mode: "open" })
        styleElements.forEach(_ => component.shadowRoot.appendChild(_))
        component.shadowRoot.appendChild(container)

        return container
    }
}