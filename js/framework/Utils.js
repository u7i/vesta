/**
 * Throws an exception of the certain type with certain text if condition is true
 */
const throw_if = (cond, type, text) => {
    if (cond)
        throw new type(text)
}