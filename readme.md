![logo](resources/logo-dark.png)

Website for the vesta project.  
Uses pure js/bootstrap as a css framework.  
Doesn't require a DB ( this site wasn't designed to hold any dynamic content ).

Content that belongs to the site:
- js/
- html/
- css/
- resources/
- index.html
- robots.txt  